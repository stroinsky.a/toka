const path = require( 'path' )
const http = require( 'http' )
const express = require( 'express' )
const { Server } = require( 'socket.io' )
const webSocket = require( './web-socket.js' )

const app = express()
const server = http.createServer( app )
const io = new Server( server, {
    cors: {
        origin: '*',
    }
} )

const PORT = process.env.PORT || 3000
const PUBLIC_PATH = path.join( process.cwd(), 'public' )

app.use( express.static( PUBLIC_PATH ) )

app.get( '*', ( req, res ) => {
    res.sendFile( PUBLIC_PATH + '/index.html' )
} )

server.listen( Number( PORT ), () => {
    webSocket(io)
    console.log( `listening on *:${ PORT }` )
} )
