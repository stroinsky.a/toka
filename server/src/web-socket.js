const { getList } = require( './handlers/get-people-list.js' )

module.exports = ( io ) => {

    io.on( 'connection', ( socket ) => {
        socket.on( 'scanner.start', ( socketData ) => startScanner( socketData, socket ) )
    } )

}

async function startScanner( socketData, socket ) {

    const { limit } = socketData

    const list = await getList( limit )
    // emit scanner.dr action with data
    socket.emit('scanner.dr', list.data)
}



