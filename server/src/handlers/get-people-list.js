const faker = require( 'faker' )

exports.getList = async ( limit ) => {

    const list = [ ...Array( limit ).keys() ].reduce( ( accumulator, index ) => {

        let id = faker.datatype.uuid()
        // Persona model
        accumulator[ id ] = {
            id:        id,
            firstName: faker.name.firstName(),
            lastName:  faker.name.lastName(),
            city:      faker.address.city(),
            criteria:  'Criteria ' + faker.datatype.number( { 'min': 1, 'max': 3 } ),
        }
        return accumulator
    }, {} )

    return {
        data:  list,
        total: faker.datatype.number( { 'min': 100, 'max': 10000 } ),
    }
}
