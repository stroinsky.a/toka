# Scanner

>How to

1. git clone the repo
2. run npm/yarn start from the root directory
    - Installs npm modules for ./server & ./client
    - Builds client in puts into ./server/public
    - Runs server on :3000 to serve Vue app
3. Go to localhost:3000

>General notes

Time estimation of "couple of hours" is fun, but is wrong. To complete this task a little 
more than a couple of hours needed. 

Docker deployment, tests are not implemented since the work hours total a lot more
than expected. Please respect time of other people and either say the task will take 
time or give tasks that require a little fewer things to complete. especially if you want
pixels to be measured and positioned according to design. 




