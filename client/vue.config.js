// vue.config.js
const { aliases } = require( './webpack.aliases.js' )

module.exports = {
    publicPath:       '/',
    configureWebpack: {
        resolve: {
            alias: aliases
        },

    }
}
