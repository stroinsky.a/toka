const path = require('path')

const resolveFromRoot = (dir) => path.resolve(process.cwd(), dir)

const ALIASES = {
    '@':     resolveFromRoot('src'),
    'UI': resolveFromRoot('src/@ui/'),
    'Assets': resolveFromRoot('src/@assets/'),
    'Core':     resolveFromRoot('src/@core/'),
    'Utils': resolveFromRoot('src/@core/utils/'),
    'Store': resolveFromRoot('src/@core/store/'),
}

module.exports = {
    resolve: {
        alias: ALIASES
    }
}

module.exports.aliases = ALIASES
