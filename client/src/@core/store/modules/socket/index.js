import setProp              from 'Store/utils/set-prop-mutation.js'
import actions, { ACTIONS } from './_actions.js'

const NAMESPACE = 'socket'

const initialState = () => {
    return {
        isConnected: false,
        hasError:    false,
    }
}

const Socket_Module = {
    namespaced: true,
    state:      initialState(),
    actions:    actions,
    mutations:  {
        setProp
    }

}

export {
    NAMESPACE,
    Socket_Module,
    ACTIONS as SocketActions,
}
