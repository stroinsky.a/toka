import { useWebSocket } from 'Core/plugins/web-socket.js'
import { sleep } from 'Core/utils';

export const ACTIONS = {
    CONNECT_TO_WS: `ws.ctw`,
}

async function connectToWebSocket( { commit } ) {

    const { connect } = useWebSocket()

    await sleep(2000) // simulate delay

    connect()
        .then( () => commit( 'setProp', { prop: 'isConnected', value: true } ) )
        .catch( () => commit( 'setProp', { prop: 'hasError', value: true } ))
}

export default {
    [ ACTIONS.CONNECT_TO_WS ]: connectToWebSocket
}
