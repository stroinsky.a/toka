import { sleep }                                       from 'Core/utils'
import { socket }                                      from 'Core/plugins/web-socket.js'
import { eventBus }                                    from 'Core/plugins/event-bus.js'
import { SocketActions, NAMESPACE as SocketNameSpace } from 'Store/modules/socket'
import { makeFilters }                                 from './helpers/make-filters.js'

export const ACTIONS = {
    SCANNER_INIT:          `scanner.init`,
    SCANNER_START:         `scanner.start`,
    SCANNER_DATA_RECEIVED: `scanner.dr`,
    SCANNER_FILTER_CHANGE: 'scanner.fc'
}

async function initializeScanner( { commit, dispatch, state } ) {

    if ( !state.hasInitialised ) {

        if ( !socket ) {
            await dispatch( `${ SocketNameSpace }/${ SocketActions.CONNECT_TO_WS }`, null, { root: true } )
        }

        socket.on( ACTIONS.SCANNER_DATA_RECEIVED, async ( data ) => {

            await sleep( 2500 ) // simulate delay

            commit( 'setProp', { prop: 'data', value: data } )
            commit( 'setProp', { prop: 'filters', value: makeFilters( data ) } )

            eventBus.emit( ACTIONS.SCANNER_DATA_RECEIVED )

            commit( 'setProp', { prop: 'isFetching', value: false } )
        } )

        commit( 'setProp', { prop: 'hasInitialised', value: true } )
    }


}

async function startScan( { commit } ) {

    commit( 'setProp', { prop: 'isFetching', value: true } )

    eventBus.emit( ACTIONS.SCANNER_START )

    socket.emit( 'scanner.start', {
        ts:    new Date().getTime(),
        limit: 20
    } )
}

async function filterList( { commit, dispatch, state }, { value, name, filterGroup } ) {

    const filters = state.filters

    filters[filterGroup][name] = {
        ...filters[filterGroup][name],
        active: !value
    }
    commit( 'setProp', { prop: 'filters', value: filters } )

}

export default {
    [ ACTIONS.SCANNER_INIT ]:          initializeScanner,
    [ ACTIONS.SCANNER_START ]:         startScan,
    [ ACTIONS.SCANNER_FILTER_CHANGE ]: filterList,
}


