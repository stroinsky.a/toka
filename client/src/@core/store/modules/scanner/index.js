import setProp from 'Store/utils/set-prop-mutation.js';
import actions, { ACTIONS } from './_actions.js';
import getters from './_getters.js';

const NAMESPACE = 'scanner'

const initialState = () => {
    return {
        hasInitialised: false,
        data: {},
        isFetching: false,
        filters: {
            city: {},
            criteria: {}
        },
    }
}

const Scanner_Module = {
    namespaced: true,
    state:      initialState(),
    getters: getters,
    actions: actions,
    mutations: {
        setProp
    },

}


export {
    NAMESPACE,
    Scanner_Module,
    ACTIONS as ScannerActions,
}
