const getVisibleList = ( state ) => {

    const list = Object.values( state.data )
    const filters = state.filters
    const activeFilters = [
        ...Object.values( filters.city ),
        ...Object.values( filters.criteria )
    ]
        .filter( f => f.active )
        .map(f => f.name)


    if ( activeFilters.length > 0 ) {
        return list.filter( ( item ) => {
            return activeFilters.includes(item.city) || activeFilters.includes(item.criteria)
        } )

    } else {
        return list
    }
}

export default {
    getVisibleList,
}
