export function makeFilters( data ) {

    return Object.values( data ).reduce( ( accumulator, person ) => {

        if ( !accumulator.criteria[ person.criteria ] ) {
            accumulator.criteria[ person.criteria ] = {
                id:     person.criteria,
                name:   person.criteria,
                active: false
            }
        }

        if ( !accumulator.city[ person.city ] && Object.keys( accumulator.city ).length < 3 ) {
            accumulator.city[ person.city ] = {
                name:   person.city,
                active: false
            }
        }

        return accumulator
    }, { criteria: {}, city: {} } )

}
