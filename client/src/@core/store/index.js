import Vue  from 'vue'
import Vuex from 'vuex'

import { Socket_Module }  from 'Store/modules/socket'
import { Scanner_Module } from 'Store/modules/scanner'

Vue.use( Vuex )

export default new Vuex.Store( {
    modules: {
        'socket':  Socket_Module,
        'scanner': Scanner_Module
    }
} )
