import Vue from 'vue'

export default  ( state, { prop, value } ) => {
    Vue.set(state, prop, value)
}
