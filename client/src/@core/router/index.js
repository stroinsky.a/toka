import Vue       from 'vue'
import VueRouter from 'vue-router'

Vue.use( VueRouter )

const defaultLayout = () => import('UI/layouts/default-layout.vue')
const scannerLayout = () => import('UI/layouts/scanner-layout.vue')

const routes = [
    {
        path:      '/',
        component: defaultLayout,
        children:  [
            {
                path:      '',
                name:      'start',
                component: () => import('UI/views/start/start-view.vue'),
            }
        ]
    },
    {
        path:      '/scanner',
        component: scannerLayout,
        children:  [
            {
                path:      '',
                name:      'scanner',
                component: () => import('UI/views/scanner/scanner-view.vue'),
            }
        ]
    }
]

const router = new VueRouter( {
    mode: 'history',
    base: process.env.BASE_URL,
    routes
} )

export default router
