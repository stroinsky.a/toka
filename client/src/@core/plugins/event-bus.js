import mitt from 'mitt';

/**
 * General app event bus
 * @type {Emitter}
 */
export const eventBus = mitt()
