import io from 'socket.io-client';

export let socket = null;

export function useWebSocket() {

    async function connect() {
        try {
            socket = io('ws://localhost:9999')
        } catch (e) {
            throw new Error(`Could not connect to WS at ws://localhost:9999`)
        }

        console.log(socket, 'plugin')
    }

    return {
        connect,
        socket,
    }
}
